From k3d [doc](https://k3d.io/v5.4.9/#install-script)

```
## Install
$ wget -q -O - https://raw.githubusercontent.com/k3d-io/k3d/main/install.sh | bash

## Test
root@devops-hands-on-1:/home/debian# k3d version
k3d version v5.4.9
k3s version v1.25.7-k3s1 (default)

## Create volume for pv
root@devops-hands-on-1:/home/debian# mkdir /data-k3d

## Create the cluster
root@devops-hands-on-1:/# k3d cluster create devops --volume /data-k3d/:/data-k3d
WARN[0000] No node filter specified
INFO[0000] Prep: Network
INFO[0000] Created network 'k3d-devops'
INFO[0000] Created image volume k3d-devops-images
INFO[0000] Starting new tools node...
INFO[0000] Starting Node 'k3d-devops-tools'
INFO[0001] Creating node 'k3d-devops-server-0'
INFO[0001] Creating LoadBalancer 'k3d-devops-serverlb'
INFO[0001] Using the k3d-tools node to gather environment information
INFO[0001] HostIP: using network gateway 172.19.0.1 address
INFO[0001] Starting cluster 'devops'
INFO[0001] Starting servers...
INFO[0001] Starting Node 'k3d-devops-server-0'
INFO[0006] All agents already running.
INFO[0006] Starting helpers...
INFO[0006] Starting Node 'k3d-devops-serverlb'
INFO[0013] Injecting records for hostAliases (incl. host.k3d.internal) and for 2 network members into CoreDNS configmap...
INFO[0015] Cluster 'devops' created successfully!
INFO[0015] You can now use it like this:
kubectl cluster-info
```

### Kubectl

```
root@devops-hands-on-1:/# curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 46.9M  100 46.9M    0     0  25.9M      0  0:00:01  0:00:01 --:--:-- 25.9M
root@devops-hands-on-1:/# chmod +x kubectl
root@devops-hands-on-1:/# mv kubectl /usr/local/bin/kubectl
```

### Test

```
root@devops-hands-on-1:/# kubectl cluster-info
Kubernetes control plane is running at https://0.0.0.0:42059
CoreDNS is running at https://0.0.0.0:42059/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
Metrics-server is running at https://0.0.0.0:42059/api/v1/namespaces/kube-system/services/https:metrics-server:https/proxy
```

Cluster is alive


### Limit access

#### nftables

```
root@devops-hands-on-1:/# apt install nftables

# Copy nftables rules from infra/cluster/nftables.nft to /etc/nftables.nft
# Copy nftables services frm infra/cluster/nftables.service to /etc/systemd/system/nftables.service

root@devops-hands-on-1:/# systemctl status nftables
● nftables.service - nftables
     Loaded: loaded (/etc/systemd/system/nftables.service; disabled; vendor preset: enabled)
     Active: inactive (dead)
       Docs: man:nft(8)
             http://wiki.nftables.org
root@devops-hands-on-1:/# systemctl enable nftables
Created symlink /etc/systemd/system/sysinit.target.wants/nftables.service → /etc/systemd/system/nftables.service.
root@devops-hands-on-1:/# systemctl start nftables
root@devops-hands-on-1:/# systemctl status nftables
● nftables.service - nftables
     Loaded: loaded (/etc/systemd/system/nftables.service; enabled; vendor preset: enabled)
     Active: active (exited) since Fri 2023-05-05 12:37:38 UTC; 13s ago
       Docs: man:nft(8)
             http://wiki.nftables.org
    Process: 29502 ExecStart=/usr/sbin/nft -f /etc/nftables.nft (code=exited, status=0/SUCCESS)
   Main PID: 29502 (code=exited, status=0/SUCCESS)
        CPU: 7ms
```

## Users

apply role and clusterrole defined in `infra/cluster/manifests/*`

Create certificates (see users.md note)


Test to see secret and pods as Paul user

```
# configure your kubectl config with certificate previously created
root@devops-hands-on-1:~# kubectl config use-context paul
Switched to context "paul".
root@devops-hands-on-1:~# kubectl get role
Error from server (Forbidden): roles.rbac.authorization.k8s.io is forbidden: User "paul.support" cannot list resource "roles" in API group "rbac.authorization.k8s.io" in the namespace "default"
root@devops-hands-on-1:~# kubectl get pods
No resources found in default namespace.
root@devops-hands-on-1:~# kubectl get secrets
Error from server (Forbidden): secrets is forbidden: User "paul.support" cannot list resource "secrets" in API group "" in the namespace "default"
```

