From gitlab [doc](https://docs.gitlab.com/runner/install/linux-repository.html)

```
$ curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
$ sudo apt-get install gitlab-runner
```
