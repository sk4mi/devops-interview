 This document aims to contain all answers about technical choices

### 1. a)

#### Lint

As code base is in python, I decide to use pylint.
Pylint is widely customizable, and simple to deploy, that's why I choose it.

#### SAST

After reading Gitlab CI/CD documentation, I found that they share a SAST template which analyze code using semgrep.
It permits to analyze wide variety of languages, and upload an artifact with results which can be downloaded after a pipeline.

### 2. a)

#### Kubernetes

I decide to use k3d to deploy a cluster on the VM.
K3d is a great choice when we want to deploy a cluster similar to a production one.
K3d is based on k3s but deploy it on docker.
I also choose k3d because it's lighweight and pretty simple to run a cluster for dev/test purpose.

To secure cluster from outside, I decide to deploy ntfable firewall directly on host. 
Like this, the cluster access can be configured at a low level with a infrastructure manager (like ansible, terraform, etc)

### 3.

To run backup, I decide to use k8s cronJob, it permis to run a simple command
to dump database and add file to persistent volume in a clean kubernetes way.
Then we can also add a step which save the dump.
For restore after crash, persistent volume should to the job. But if data is
loss, we can enter on postgresql pod and restore the dump.
