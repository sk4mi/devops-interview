from https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/#normal-user

# Generate private key

```
root@devops-hands-on-1:~# openssl genrsa -out peter-backend.key 2048
Generating RSA private key, 2048 bit long modulus (2 primes)
..............................................+++++
...+++++
e is 65537 (0x010001)
root@devops-hands-on-1:~# openssl req -new -key peter-backend.key -out peter-backend.csr
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:developer
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:peter.backend
Email Address []:peter.backend@pasqal.test

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
root@devops-hands-on-1:~#
root@devops-hands-on-1:~#
root@devops-hands-on-1:~#
root@devops-hands-on-1:~# openssl genrsa -out  paul-support.key 2048
Generating RSA private key, 2048 bit long modulus (2 primes)
.................+++++
..............................................................................................................................................................+++++
e is 65537 (0x010001)
root@devops-hands-on-1:~# openssl req -new -key paul-support.key -out paul-support.csr
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:guest
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:paul.support
Email Address []:paul.support@pasqal.test

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
root@devops-hands-on-1:~#
root@devops-hands-on-1:~#
root@devops-hands-on-1:~#
root@devops-hands-on-1:~# openssl genrsa -out  mary-devops.key 2048
Generating RSA private key, 2048 bit long modulus (2 primes)
............................................+++++
....................................................+++++
e is 65537 (0x010001)
root@devops-hands-on-1:~# openssl req -new -key mary-devops.key -out mary-devops.csr
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:maintainer
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:mary.devops
Email Address []:mary.devops@pasqal.test

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
```

# Create CSR

Peter CSR

```
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: peter.backend
spec:
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ3ZUQ0NBYVVDQVFBd2VERUxNQWtHQTFVRUJoTUNRVlV4RXpBUkJnTlZCQWdNQ2xOdmJXVXRVM1JoZEdVeApFakFRQmdOVkJBb01DV1JsZG1Wc2IzQmxjakVXTUJRR0ExVUVBd3dOY0dWMFpYSXVZbUZqYTJWdVpERW9NQ1lHCkNTcUdTSWIzRFFFSkFSWVpjR1YwWlhJdVltRmphMlZ1WkVCd1lYTnhZV3d1ZEdWemREQ0NBU0l3RFFZSktvWkkKaHZjTkFRRUJCUUFEZ2dFUEFEQ0NBUW9DZ2dFQkFMeUdUZU1uVDY0eXo1TEQ0OFVoblVZeWVxbzFEeEJYOGlFbApiTDVzWmVyZHF3cjNyRzJvdlZobmp5OUtMNVZaRkRGK24rT096QUhQWGtVNTltSXJzblIyUEptckhtR2VFbmVxCkNNTDhRQm16b3RFUXUzOG1tQnQ1NHpLZFlVYzk4b2tOaDliMm5GRXF6WDFDbTZHcTJFVnJVQ0dYVVgrMHhCYlUKNTFaRUU5LzN5eHU2cHVqQ1UxcXpFaG1aTUtVQTF6RFdVTXZyYVlDajFIS3A3NWgxNm8vTVdjd1BQR3pVaGhidwpZWm1LOENOTkpVRjVhdHpzSmc2cmZqZkdDT1RyZ1NLamM4eDJmMTVvd2N6aXBySDFMYXpmdnpGQUlYOFNmQVZaClJDa2ZWbSs0RnNIaitzNXorSExxeHR5eGxPUzJIRllmTk10UFB0N1FkWCtpeEFPQ2R1MENBd0VBQWFBQU1BMEcKQ1NxR1NJYjNEUUVCQ3dVQUE0SUJBUUJ4VExSR3g4a1dFWVZBUVlveHZwaE84Z0txOUNmNlRUenJsTXdmMmZjcQo5TzdWR3VlMUtXclR3Ymx0OXU1V3lFYUEvSzFyamZaQ1cyOEVMM1R5WEtMcDN5SnZWYnZiZ2Yyem1MNFI0M0RiCmJJT1k0enVDVTY1TW9lZG9KQ0dORmRmOURTQlpvNS8waGlJdlRteVJxMFJEZERaQk82QW1VK1JEaTRCK29pQnkKUGU3bDJ3YkhIK2JjeFlkUDVKSDVNVHQvNFI2VVhsek1jWCtNcXZzb3VFYXhsbW9MTXdGQXo2cHBRd1owVnpuUAo0QXBuNFhML0dQTFNRZDZBSCtyUSs3RFpLMzlmUDlvTG9XVDhzbkt1Q3hCUDNYU2lDUEdmYWxyenI5WHl3WG9UCjJmeDJHckhkTnRtTHhYWmNLYk1WdHNlbTNOMzlHSGRhRTJOcG1iN0lsTzgzCi0tLS0tRU5EIENFUlRJRklDQVRFIFJFUVVFU1QtLS0tLQo=
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 86400  # one day
  usages:
  - client auth
```

Paul CSR

```
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: paul.support
spec:
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ3R6Q0NBWjhDQVFBd2NqRUxNQWtHQTFVRUJoTUNRVlV4RXpBUkJnTlZCQWdNQ2xOdmJXVXRVM1JoZEdVeApEakFNQmdOVkJBb01CV2QxWlhOME1SVXdFd1lEVlFRRERBeHdZWFZzTG5OMWNIQnZjblF4SnpBbEJna3Foa2lHCjl3MEJDUUVXR0hCaGRXd3VjM1Z3Y0c5eWRFQndZWE54WVd3dWRHVnpkRENDQVNJd0RRWUpLb1pJaHZjTkFRRUIKQlFBRGdnRVBBRENDQVFvQ2dnRUJBSlVNYkJRU2lyTEZjMDNWRWxoZ1NWcFdTdzlZdW9IT3llelN6NzZkMlE3eAozUHFOMnFWd3NtV0dPem9vWVJsSXNId016czhieFQ3TlFab0RlRDNMRmhoSStpM3JVYVczQzM5MlZ4cXhTTXhFCmZmbllTL3pyOUdaRUwvdEllbitjeXR6OFV4Q0dzeEw0a0Y3MDRJNTYybExzNm85ZXdwc01UN2liT0FHblhOQWgKcEVpeEc0clNPSDg2S1dMeitNdEk0NzBBNWpEMkNlWm56cE5Nc0JZb0Jvc05KSzJvOWJUZmduTndFTDFwUEYrUApEZzVJY2hkQW9lVm5Yb0s5Z2szRHRwQXhhWXVFV0R1NmVUV1Q2Rkt4OXN3dGg3d2c0K0FRdlJURFNaMDF4UzcrCkhDdUY1eWJXUnhrejlnNnRJWTBPUnFCSlM5anBRQU1rTWVvUW00WmdLbjhDQXdFQUFhQUFNQTBHQ1NxR1NJYjMKRFFFQkN3VUFBNElCQVFCUmRXRjdmdmZVV3pUdnNSL21GUmJ2YmhpUEo1dmowZEJNa1RLaVErZC9sK0o0NzU5NApTOG4zU1UwalJqM0I0TGJnUCtvVi9FZjR5VXBWRU9sQmkzamF1MlIxZ1lxUTBOdUlablM1MDRKbHgwVVNveFY2CnljVm1XUzdUTFlHd2laL3UrY0hQclRmcnlqMmVpQ2xPZzJoR25qUnhKKzJXMUg2eThIc2F0WnB2RW9rQ1NNekwKMFloUlRwTmJNT2RIVHM4WEFjVWZ6MkVOZHI4ZndKcFNoU0VoTnVlNWMwWEtWbEhkSUxhWEJmWU5vdzQxV0oxVwpEZndSa1gyaFRHK2RWdGh0VlRiTllTK1p6UGpYaHJHSElGbC8rSTcxYXg4YWovWnR0UWoyNjhJMUR0bzVzZXRrCkpoczBIRG9QNXZGaHlHNkZHQ2NEZTZSVUJZZEF4UUdNazdabwotLS0tLUVORCBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0K
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 86400  # one day
  usages:
  - client auth
```

Mary CSR

```
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: mary.devops
spec:
  request: LS0tLS1CRUdJTiBDRVJUSUZJQ0FURSBSRVFVRVNULS0tLS0KTUlJQ3VqQ0NBYUlDQVFBd2RURUxNQWtHQTFVRUJoTUNRVlV4RXpBUkJnTlZCQWdNQ2xOdmJXVXRVM1JoZEdVeApFekFSQmdOVkJBb01DbTFoYVc1MFlXbHVaWEl4RkRBU0JnTlZCQU1NQzIxaGNua3VaR1YyYjNCek1TWXdKQVlKCktvWklodmNOQVFrQkZoZHRZWEo1TG1SbGRtOXdjMEJ3WVhOeFlXd3VkR1Z6ZERDQ0FTSXdEUVlKS29aSWh2Y04KQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUw3enlvQW9qMUw5VnMwUmVQNXRGcEJaa3oxdFFpc3Urais1SHBhegp5RktpN3BTZFpwVzRIakcvOE8xVWF6eEsvbEVTTjRIazM2OWMrZDRpZE9XY0RPcit3WCt1Wm81OFdrSTNXaElvCkRxQmt5ZGhtREpVUHZRWkIxZ2pRNzVFWjhoUG1wSmd1ZG9yZkcvSmEvOW92N2FyanRBYlVrT25mVGVoUHVLWWUKdCsyMDRiZGQzeDZ5RTZGQlhDT0piM090QXNIbFgrOG1BM0x2OVBvZW01ZnZDdXNla3JZK2JuOFdJQjdiUSt4dQplek9lc2xqdlI2Y2ZGdHlZMnRuVGgzVkhpTzFRbCszSEhDb2xmTWE3bjFNZDBOMUhCQk45bUxTbGNSRFdFaExPCnYrY2RqT1JjanNFTUdiRC9LeEtrTTQzSUViNmZIVFJpZndtbmtKWEdFYldtdFE4Q0F3RUFBYUFBTUEwR0NTcUcKU0liM0RRRUJDd1VBQTRJQkFRQS95dlJJSE5raUtBck1TbVRlb2ZrYTViZ2Q1Q0pKOEx0aE9UNGZjamZyMFo4MwpHaS9EMENlVE9UbC9SOHQ3U051VGdnYkVhSU8yb0VreFhER0lGbXpXWmpjcUl2cVlMd01FVjZhSzRQbkdaZzd3Ck5EUHNweDUwSG9vSUN6UDhHSkV1YlpXS3ZMYUw3L0Vud2ZkQUJuUlBCMFhKODJ3V0phdS80QnFHZGRab08wUlkKNjVxa2R5WmhDc2FGMUU0cm5xVXdFdHlzcVRGYzdNdVNqWjJIczA2Y3lXMy80U0hob0xYM1I0ZmZnbjBUbzR1SQo2L21rd2hqc2k4NkgrL29rMGQrdXRYSm45VmpUcGZsZENueDcwMWoyYnRkeEx4SmdDNEIwY0hzMm43djE5M2RlCkNCUXhNbTJ2c1p3dlp5SVBCSnI1YmZyeGcvVlVJakFXL3RhWnFtNlIKLS0tLS1FTkQgQ0VSVElGSUNBVEUgUkVRVUVTVC0tLS0tCg==
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 86400  # one day
  usages:
  - client auth
```

After apply these files, we need to approve csr with `kubectl certificate approve <CSR_NAME>`


Then we're good
```
root@devops-hands-on-1:~# kubectl get csr
NAME            AGE   SIGNERNAME                            REQUESTOR      REQUESTEDDURATION   CONDITION
peter.backend   67s   kubernetes.io/kube-apiserver-client   system:admin   24h                 Approved,Issued
paul.support    35s   kubernetes.io/kube-apiserver-client   system:admin   24h                 Approved,Issued
mary.devops     22s   kubernetes.io/kube-apiserver-client   system:admin   24h                 Approved,Issued
```

We can now get the certificate to give to user

```
root@devops-hands-on-1:~# kubectl get csr mary.devops -o jsonpath='{.status.certificate}'| base64 -d > mary-devops.crt
root@devops-hands-on-1:~# kubectl get csr paul.support -o jsonpath='{.status.certificate}'| base64 -d > paul-support.crt
root@devops-hands-on-1:~# kubectl get csr peter.backend -o jsonpath='{.status.certificate}'| base64 -d > peter-backend.crt
```


