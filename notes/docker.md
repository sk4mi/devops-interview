From docker [doc](https://docs.docker.com/engine/install/debian/#install-using-the-repository)

```
root@devops-hands-on-1:/home/debian# apt-get update
root@devops-hands-on-1:/home/debian# apt-get install ca-certificates curl gnupg
root@devops-hands-on-1:/home/debian# install -m 0755 -d /etc/apt/keyrings
root@devops-hands-on-1:/home/debian#  curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
root@devops-hands-on-1:/home/debian# chmod a+r /etc/apt/keyrings/docker.gpg
root@devops-hands-on-1:/home/debian# echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
root@devops-hands-on-1:/home/debian# apt-get update
root@devops-hands-on-1:/home/debian#  sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

```
root@devops-hands-on-1:/home/debian# docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

# OK
```

Add gitlab-runner user to docker group
```
usermod -aG docker gitlab-runner
```
