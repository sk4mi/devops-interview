from random import randint
from time import sleep

from flask import Flask
from flask import request
from jaeger_client import Config
from opentracing.propagation import Format
import time

app = Flask(__name__)
config = Config(
    config={
        'sampler':{ 
            'type': 'const',
            'param': 1
        },
        'local_agent': {
            'reporting_host': 'jaeger-tracing-agent.monitoring',
            'reporting_port': '5775',
        },
        'logging': True,
    }, 
    service_name="backend_dog")
jaeger_tracer = config.initialize_tracer()

counter_value = 2

def get_counter():
    return str(counter_value)

def increase_counter():
    global counter_value
    int(counter_value)
    sleep(randint(1,10))
    counter_value += 1
    return str(counter_value)

@app.route('/api/counter/dog', methods=['GET'])
def counter():
    time.sleep(0.6)
    span_ctx = jaeger_tracer.extract(Format.HTTP_HEADERS, request.headers)
    with jaeger_tracer.start_active_span('counter dogs', child_of=span_ctx) as child_span:
        child_span.span.set_tag("current_service", "dog")
        child_span.span.log_kv({'event': 'request.method GET', 'path': '/api/counter/dog'})
        return increase_counter()