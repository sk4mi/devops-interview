from random import randint
from time import sleep

from flask import Flask
from flask import request
from jaeger_client import Config
from flask_opentracing import FlaskTracing
import time
from opentracing.propagation import Format

app = Flask(__name__)
config = Config(
    config={
        'sampler':{ 
            'type': 'const',
            'param': 1
        },
        'local_agent': {
            'reporting_host': 'jaeger-tracing-agent.monitoring',
            'reporting_port': '5775',
        },
        'logging': True,
    }, 
    service_name="backend_cat")
jaeger_tracer = config.initialize_tracer()

counter_value = 4

def get_counter():
    return str(counter_value)

def increase_counter():
    global counter_value
    int(counter_value)
    sleep(randint(1,10))
    counter_value += 1
    return str(counter_value)

@app.route('/api/counter/cat', methods=['GET'])
def cat_counter():
    time.sleep(0.4)
    span_ctx = jaeger_tracer.extract(Format.HTTP_HEADERS, request.headers)
    with jaeger_tracer.start_active_span('counter cat', child_of=span_ctx) as scope:
        
        scope.span.log_kv({'event': 'request.method GET', 'path': '/api/counter/cat'})
        return increase_counter()