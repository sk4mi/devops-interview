docker build -t anneclairelh/backend-animal:latest backend-animal/
docker push anneclairelh/backend-animal:latest
docker build -t anneclairelh/frontend-animal:latest frontend-animal/
docker push anneclairelh/frontend-animal:latest
docker build -t anneclairelh/backend-cat:latest backend-cat/
docker push anneclairelh/backend-cat:latest
docker build -t anneclairelh/backend-dog:latest backend-dog/
docker push anneclairelh/backend-dog:latest
docker build -t anneclairelh/backend-todo:latest backend-todo/
docker push anneclairelh/backend-todo:latest