import requests
import opentracing
from flask import Flask
from flask import request
from jaeger_client import Config
import time
from opentracing.propagation import Format

app = Flask(__name__)
config = Config(
    config={
        'sampler':{ 
            'type': 'const',
            'param': 1
        },
        'local_agent': {
            'reporting_host': 'jaeger-tracing-agent.monitoring',
            'reporting_port': '5775',
        },
        'logging': True,
    }, 
    service_name="backend_animal")
tracer = config.initialize_tracer()

@app.route('/api/counter/animal', methods=['GET', 'POST'])
def animal_counter():
    time.sleep(1)
    span_ctx = tracer.extract(Format.HTTP_HEADERS, request.headers)
    text_carrier = {}
    tracer.inject(span_ctx, opentracing.Format.HTTP_HEADERS, text_carrier)

    headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "uber-trace-id": text_carrier["uber-trace-id"]
    }

    with tracer.start_span('backend animal', child_of=span_ctx) as child_span:
        text_carrier = {}
        tracer.inject(child_span.context, opentracing.Format.HTTP_HEADERS, text_carrier)
        headers["uber-trace-id"] = text_carrier["uber-trace-id"]

        #set Tags
        child_span.set_tag("current_service", "animal")

        #call dog api
        child_span.log_kv({'msg': 'call to dog api'})
        counter_nice_dogs = requests.get("http://do-visit-counter-backend-dog:5011/api/counter/dog",headers=headers)
        
        #call cat api
        child_span.log_kv({'msg': 'call to cat api', "par": '2' })
        counter_naughty_dogs = requests.get("http://do-visit-counter-backend-cat:5012/api/counter/cat",headers=headers)
        
        return f""" {counter_nice_dogs.text} Dogs \n\n {counter_naughty_dogs.text} Cats  \n\n"""