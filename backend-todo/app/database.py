from pyaml_env import parse_config

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


config = parse_config("./config.yaml")
db_config = config["db"]

SQLALCHEMY_DATABASE_URL = (
    f"postgresql://{db_config['username']}:{db_config['password']}@postgres:5432/app"
)
engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
