from typing import Union

from pydantic import BaseModel


class TodoBase(BaseModel):
    item: str


class TodoCreate(TodoBase):
    pass


class Todo(TodoBase):
    id: int
    item: str

    class Config:
        orm_mode = True