from sqlalchemy.orm import Session

from . import models, schemas


def get_todos(db: Session):
    match = db.query(models.Todo).all()
    return match


def get_todo_by_id(db: Session, id: int):
    return db.query(models.Todo).filter(models.Todo.id == id).first()


def add_todo(db: Session, todo: schemas.TodoCreate):
    todo_model = models.Todo(item=todo.item)
    db.add(todo_model)
    db.commit()
    db.refresh(todo_model)
    return


def update_todo(db: Session, id: int, changes: dict):
    match = db.query(models.Todo).filter(models.Todo.id == id).first()
    if match:
        db.query(models.Todo).filter(models.Todo.id == id).update(changes)
        db.commit()
    return match


def delete_todo(db: Session, id: int):
    match = db.query(models.Todo).filter(models.Todo.id == id).first()
    if match:
        db.query(models.Todo).filter(models.Todo.id == id).delete()
        db.commit()
    return match