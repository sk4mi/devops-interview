from typing import Dict, List, Union

from fastapi import Depends, FastAPI
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.orm import Session


from . import crud, models, schemas
from .database import SessionLocal, engine


models.Base.metadata.create_all(bind=engine)

app = FastAPI()

origins = ["http://localhost:3000", "localhost:3000"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Database dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/", tags=["root"])
async def read_root() -> dict:
    return {"message": "Welcome to your todo list."}


@app.get(
    "/todo", tags=["todos"], response_model=Dict[str, Union[List[schemas.Todo], None]]
)
async def get_todos(db: Session = Depends(get_db)) -> dict:
    return {"data": crud.get_todos(db)}


@app.post("/todo", tags=["todos"])
async def add_todo(todo: schemas.TodoCreate, db: Session = Depends(get_db)) -> dict:
    crud.add_todo(db, todo)
    return {"data": {"Todo added."}}


@app.put("/todo/{id}", tags=["todos"])
async def update_todo(id: int, changes: dict, db: Session = Depends(get_db)) -> dict:
    todo = crud.update_todo(db, id, changes)
    if todo:
        return {"data": f"Todo with id {id} has been updated."}
    return {"data": f"Todo with id {id} not found."}


@app.delete("/todo/{id}", tags=["todos"])
async def delete_todo(id: int, db: Session = Depends(get_db)) -> dict:
    todo = crud.delete_todo(db, id)
    if todo:
        return {"data": f"Todo with id {id} has been removed."}
    return {"data": f"Todo with id {id} not found."}
