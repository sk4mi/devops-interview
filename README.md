# DevOps Engineer Interview (PASQAL Cloud Services)

## Context & Project Structure

An intern in Pasqal Cloud Services team created a platform to help him manage his pet park. This platform is composed of multiple microservices:

- a backend-cat microservice, in charge of counting cats in the park. It is a Flask REST API.
- a backend-cat microservice, in charge of counting dogs in the park. It is a Flask REST API.
- a backend-animal microservice, which counts all animals in the park by calling the two microservices above. It is a Flask REST API.
- a frontend-animal microservice, which is a user interface. It is also powered by Flask.
- a backend-todo microservice, which is a new addition and helps keep track of the maintenance needed for the park. It is FastAPI REST API. It writes/reads from a Postgres database.

However, the CI/CD processes for this platform are incomplete. Here is a quick note from the intern summarizing what he's done on this side so far:

- Docker files for all microservices
- a docker compose file for launching `backend-todo` and its database.
- Kubernetes manifests for each of the animal microservices.

The overall purpose of this exercise is to clean up and build on this work. You will find the detailed questions in the PDF that was sent to you.

### Source

Part of this code was built on top of the code from this blog [post](https://testdriven.io/blog/fastapi-react/).

### Quick start - run and test

#### Todo service

1. Run the server-side FastAPI app in one terminal window:

   ```sh
   $ python3.9 -m venv env
   $ source env/bin/activate
   (env)$ pip install -r requirements.txt
   (env)$ pip3 install psycopg2-binary
   (env)$ export POSTGRES_USERNAME=postgres
   (env)$ export POSTGRES_PASSWORD=foo
   (env)$ python backend-todo/main.py
   ```

   Navigate to [http://localhost:8000](http://localhost:8000)

2. Run the database

   ```sh
   $ docker run --name postgres -d -e POSTGRES_PASSWORD=foo -p 5432:5432 postgres
   $ docker exec -it postgres psql -U postgres
   ```

Alternatively, run using docker compose

```sh
$ docker compose -f docker-compose-todo.yaml pull
$ docker compose -f docker-compose-todo.yaml up -d --remove-orphans
```

3. You can then quickly test the API, for example with the following `curl` command:
   ```sh
   $ curl --request POST 'localhost:8000/todo' --header 'Content-Type: application/json' -d'{"item":"sequence"}' # Create a todo
   $ curl --request PUT 'localhost:8000/todo/1' --header 'Content-Type: application/json' -d'{"item":"sequence"}' # Edit a todo
   $ curl --request GET 'localhost:8000/todo' # List todos
   ```

#### Animal service

1. To run every microservice locally (for example frontend-animal), do:

   ```sh
   $ docker pull anneclairelh/frontend-animal:latest
   $ docker run -d -p 8000:8000 anneclairelh/frontend-animal:latest
   ```

2. Visit the frontend at: http://localhost:8000.
