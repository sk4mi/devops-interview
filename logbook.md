# Devops Pasqal interview logbook

- [13h11] Test connection to vm
- [13h13] Init repository
- [13h15] Init CI/CD
- [13h32] Installing gitlab-runner on VM
- [13h45] Install docker engine on VM
- [13h56] Fix dependencies for pylint job
- [14h04] Patch lint job to allow failure
- [14h15] Install kubernetes stuff
- [14h30] Deploy nftables on VM
- [14h45] Configure roles on k8s cluster
- [15h00] Starting debug nftables that drop docker traffic
- [15h20] Disable nftables to let me continue test
          /!\ Need to find a solution for docker traffic complexity
- [15h21] Create user certificate
- [15h40] Create rolebinding
- [16h09] Start automate deploymemt
- [16h45] Create helm chart for remaining apps
- [17h00] Create cronjob for pgsql backup
