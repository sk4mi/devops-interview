import os
from random import randint
from time import sleep
import requests
from flask import Flask
from jaeger_client import Config
from flask_opentracing import FlaskTracing
from opentracing.propagation import Format
import opentracing
from opentracing_instrumentation.request_context import get_current_span, span_in_context

app = Flask(__name__)
config = Config(
    config={
        'sampler':{ 
            'type': 'const',
            'param': 1
        },
        'local_agent': {
            'reporting_host': 'jaeger-tracing-agent.monitoring',
            'reporting_port': '5775',
        },
        'logging': True,
    }, 
    service_name="do_visit_counter_service_frontend")
tracer = config.initialize_tracer()

counter_value_man = 1
counter_value_woman = 2
counter_value_child = 2

def increase_counter_man():
    with tracer.start_span('increase_counter_man', child_of=get_current_span()) as span:
        with span_in_context(span):
            global counter_value_man
            int(counter_value_man)
            sleep(randint(1,5))
            counter_value_man += 1
            return str(counter_value_man)

def increase_counter_woman():
    with tracer.start_span('increase_counter_woman', child_of=get_current_span()) as span:
        with span_in_context(span):
            global counter_value_woman
            int(counter_value_woman)
            sleep(randint(1,5))
            counter_value_woman += 1
            return str(counter_value_woman)

def increase_counter_child(span):
    span.log_kv({'function': 'increase_counter_child', 'event': 'start'})
    global counter_value_child
    int(counter_value_child)
    sleep(randint(1,5))
    counter_value_child += 1
    span.log_kv({'function': 'increase_counter_child', 'event': 'end', 'value': counter_value_child})
    return str(counter_value_child)

@app.route('/')
def hello_world():
    with tracer.start_span('hello-world') as span:
        with span_in_context(span):
            men_nb = increase_counter_man()
            women_nb = increase_counter_woman()
            children_nb = increase_counter_child(span)
            return f"""Hello, World! {men_nb} men , {women_nb} women, {children_nb} children !\n\n"""

@app.route('/animals')
def animals():
    with tracer.start_span('frontend') as span:
        headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "uber-trace-id": ""
        }

        text_carrier = {}
        tracer.inject(span.context, opentracing.Format.HTTP_HEADERS, text_carrier)
        headers["uber-trace-id"] = text_carrier["uber-trace-id"]

        res = requests.get("http://do-visit-counter-backend-animal:5010/api/counter/animal", headers=headers)
        return f"""{res.text}"""
